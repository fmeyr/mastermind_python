import tkinter 
import random
import collections

class Mindmania:
    def __init__(self, principal):
        self.principal = principal
        self.interface = tkinter.Canvas(principal)
        self.etat = tkinter.Label(principal)
        self.tableau()
    def tableau(self, event=None):
        self.interface.destroy()
        self.etat.destroy()
        self.interface = tkinter.Canvas(self.principal, width=800, height=950)
        self.interface.pack()
        self.selection = {'r':self.interface.create_rectangle(5, 950, 56, 900, fill='red', outline='red'),
                    'o':self.interface.create_rectangle(71, 950, 121, 900, fill='orange', outline='orange'),
                    'y':self.interface.create_rectangle(136, 950, 190, 900, fill='yellow', outline='yellow'),
                    'g':self.interface.create_rectangle(205, 950, 257, 900, fill='green', outline='green'),
                    'b':self.interface.create_rectangle(272, 950, 324, 900, fill='blue', outline='blue'),
                    'p':self.interface.create_rectangle(339, 950, 390, 900, fill='purple', outline='purple')
                   }
        self.identifiantselec = {v:k for k,v in self.selection.items()}
        self.couleurs = {'r':'red', 'o':'orange', 'y':'yellow',
                       'g':'green', 'b':'blue', 'p':'purple'}
        self.reponses = ['']
        self.etat = tkinter.Label(self.principal)
        self.etat.pack()
        self.interface.bind('<1>', self.verification)
        self.modele = [random.choice('roygbp') for _ in range(4)]
        self.decompte = collections.Counter(self.modele)
    def verification(self, event=None):
        id = self.interface.find_withtag("current")[0]
        guess = self.identifiantselec[id]
        self.reponses[-1] += guess
        x_offset = (len(self.reponses[-1]) - 1) * 70
        y_offset = (len(self.reponses) - 1) * 90
        self.interface.create_rectangle(x_offset, y_offset,
                                x_offset+50, y_offset+50,
                                fill=self.couleurs[guess],
                                outline=self.couleurs[guess])
        if len(self.reponses[-1]) < 4:
            return
        Decomptereponses = collections.Counter(self.reponses[-1])
        Fermeture = sum(min(self.decompte[k], Decomptereponses[k]) for k in self.decompte)
        Validation = sum(a==b for a,b in zip(self.modele, self.reponses[-1]))
        Fermeture -= Validation
        couleurs = Validation*['black'] + Fermeture*['white']
        positionindices = [(320, y_offset, 343, y_offset+23),
                           (360, y_offset, 383, y_offset+23),
                           (320, y_offset+29, 343, y_offset+52),
                           (360, y_offset+29, 383, y_offset+52)]
        for couleur, position in zip(couleurs, positionindices):
            self.interface.create_rectangle(position, fill=couleur, outline=couleur)
        if Validation == 4:
            self.etat.config(text='Bien joué ! Tu as gagné')
            self.interface.unbind('<1>')
        elif len(self.reponses) > 9:
            self.etat.config(
                               text='Perdu ! La réponse était {}'.format(
                                ''.join(self.modele)))
            self.interface.unbind('<1>')
        else:
            self.reponses.append('')
        
utilisateur = tkinter.Tk()
jeu = Mindmania(utilisateur)
utilisateur.mainloop()