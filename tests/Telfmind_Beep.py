from tkinter import *
from random import *
from winsound import *
from tkinter.messagebox import  askokcancel

listecouleurpion = ['red3', 'coral', 'gold', 'SpringGreen2', 'DeepSkyBlue2', 'DarkOrchid2']
idlistecouleurpion = [0, 1, 2, 3, 4, 5]		                  
listemystere = [0,0,0,0]                                                     
listedumoment = [0, 0, 0, 0]                                               
coordcercledumoment = []                                                          
nbclicga = 0                                                               
nbessai = 1                                                             
nbclicgaparessai = 0                                                       
entraindejouer = False                                                           
cerclecouleur = []


def imagetelfmind():
    """ Image de base du jeu"""
    plateau.create_image (200, 350, image = logo)

def imagefin():
    """ Image de fin du jeu"""
    plateau.create_image (150, 600, image = vicabdef)

def menu(fenetre):     
    "Barre de menu de Telfmind"

    top=Menu(fenetre)
    fenetre.config(menu = top)
    J=Menu(top)
    top.add_cascade(label = 'Jeu', menu = J, underline = 0)
    J.add_command(label = 'Nouvelle partie', command = jouance, underline = 0)
    J.add_command(label = 'Test Ligne', command = nouveautest, underline = 0)
    J.add_command(label = 'Efface Ligne', command = nouveleffaçage, underline = 0)
    J.add_command(label='Abandonner (montrer la reponse)', command = txtabandon, underline = 0)
    J.add_command(label = 'Quitter le jeu', command = quitter, underline = 0)


def aleatoire():
        """ Définition aléatoire de la combinaison de couleur à deviner + Mise en forme: suite de 5 couleurs à deviner """
        
        idlistecouleurpion = [0, 1, 2, 3, 4, 5]										  
        plateau.create_rectangle (10, 58, 190, 92, outline = 'white',fill = 'black')
        for i in range (4):
                listemystere[i] = choice(idlistecouleurpion)        
                plateau.create_text ((i+1)*40, 76, text = "*", font = "Rockwell 16 ", fill = "white")
                idlistecouleurpion.remove (listemystere[i])
                
def colorage(event):
    if entraindejouer:
        """ Gestion de l'événement "Clic gauche", sur un cercle, pour sélectionner une couleur """

        global nbclicgaparessai
        global nbclicga
        if nbclicga >=6: nbclicga = 0
        global cerclecouleur 
        
        X = event.x
        Y = event.y
        
        for i in range (4):
                if (coordcercledumoment [i][0] <= X <= coordcercledumoment [i][2]) and (coordcercledumoment [i][1] <= Y <= coordcercledumoment [i][3]):
                        cerclecouleur.append(plateau.create_oval (coordcercledumoment[i], fill = listecouleurpion[nbclicga]))
                        listedumoment[i] = nbclicga
        nbclicga = nbclicga+1
        nbclicgaparessai = nbclicgaparessai + 1

def jouance():
        """ Gestion d'une partie avec création de cercles et bouton de test"""

        plateau.delete(ALL)
        global nbessai
        global nbclicga
        global nbclicgaparessai
        nbclicga = 0
        nbessai = 1					
        nbclicgaparessai = 0
        creercercle(nbessai)        
        aleatoire()				
        global entraindejouer
        entraindejouer = True			
        plateau.create_text(90, 32, text = "Mot de Passe", font = "Rockwell 16 ", fill = "white") 
        plateau.create_text(290, 32, text = "Indice", font = "Rockwell 16", fill = "white") 

def nouveautest():      
    """ Délenche le test de la ligne remplie ce tour"""

    global nbclicgaparessai
    global cerclecouleur
    if entraindejouer == True and nbclicgaparessai >= 4:
        global nbessai
        nbessai = nbessai+1
        if nbessai > 10:
            txtdefaite()
        creercercle(nbessai)
        indication()
        nbclicgaparessai = 0 
        cerclecouleur = []

def nouveleffaçage():
    """ Définit l'effaçage de la ligne remplie ce tour"""

    global cerclecouleur
    i = 0
    while cerclecouleur != []:
        try :
            plateau.itemconfigure (cerclecouleur[i], fill = 'slate blue', outline = 'white')
            cerclecouleur.pop (0)
        except ValueError:
            print ("la valeur est vide")

def indication():
    """ Permet d'afficher les indices pour le joueur"""

    global nbclicga
    valide = 0 
    j = 0 
    for i in range(4):
        if listedumoment[i] in listemystere :
            if listedumoment[i] == listemystere[i] :
                plateau.create_oval ((j+6)*40-15, (nbessai-1)*35+70, (j+6)*40+15, (nbessai - 1)*35+100, fill = "red")
                valide = valide + 1
                j = j + 1
            if listedumoment[i] in listemystere and listedumoment[i] != listemystere[i]:
                plateau.create_oval ((j+6)*40-15, (nbessai-1)*35+70, (j+6)*40+15, (nbessai - 1)*35+100, fill = "white")
                j = j + 1
        if valide == 4:
            txtvictoire()

def findepartie():
    """ Pour déclarer la fin de la partie et révèler la combinaison """

    global entraindejouer
    if entraindejouer == True:
        for i in range (len(listemystere)):
            plateau.create_oval ((i+1)*40-15, 60, (i+1)*40+15, 90, fill = listecouleurpion[listemystere[i]])
        entraindejouer = False 
        imagefin() 

def rejouance():
    """ Message box pour rejouer une partie"""

    repartie = askokcancel('TelfMind',"Je crois que la partie est finie ! Alors, on se fait une petite revanche ?")
    if repartie:
        jouance()

def musique_de_jeu():
    """ Musique qui sera lue durant l'execution du script et les parties jouées"""

    import winsound
    winsound.Beep(200,300)

def txtvictoire():
    """ Message délenché en cas de victoire de la partie"""

    findepartie()
    plateau.create_text(200, 720, text = "Incroyable ! Tu es vraiment le plus fort !", font = "Rockwell 13", fill = "white") 
    rejouance()

def txtdefaite():
    """ Message délenché en cas de défaite de la partie"""

    findepartie()
    plateau.create_text(200, 720, text = "Trop d'essais tue l'essai, tu sais ? Tu as perdu !", font = "Rockwell 13 ", fill = "white") 
    rejouance()

def txtabandon():
    """ Message délenché en cas d'abandon de la partie"""

    findepartie()
    plateau.create_text(200, 720, text = "Tu abandonnes ? Quel dommage... voilà ta réponse.", font = "Rockwell 12 ", fill = "white") 
    rejouance()

def creercercle(nbessai):     
    """ Création des cercles du test, avant leur coloration"""
    
    for i in range (4):
            cercle = plateau.create_oval ((i+1)*40-15, nbessai*35+70, (i+1)*40+15, nbessai*35+100, outline = "white")
            coordcercledumoment.insert (i, plateau.coords (cercle))        

def quitter():
    """ Message box pour quitter le jeu"""

    quittage = askokcancel('TelfMind',"Tu es sûr de vouloir quitter la partie ? On s'amuse bien pourtant !") 
    if quittage:
        fenetre.destroy() 
        exit()

   
fenetre = Tk()
fenetre.title('TelfMind, le plus grand de tous les Master')
plateau = Canvas(fenetre, height = 750,width = 400, bg ='slate blue' )
plateau.pack(side = RIGHT, padx = 0, pady = 0)
contreplateau = Canvas(fenetre, height = 750, width = 200, bg ='gray20')
contreplateau.pack(side = LEFT) 
selObject = contreplateau
testBouton = Button(contreplateau, text = ("Test Ligne"), command = nouveautest)
testBouton.pack(side = TOP, padx = 0, pady = 364) 
effaceBouton = Button(contreplateau, text = ("Efface Ligne"), command = nouveleffaçage)
effaceBouton.pack(side = LEFT)
menu(fenetre)
musique_de_jeu()
logo = PhotoImage(file = 'Logo.png')
vicabdef = PhotoImage(file = 'Victoire.png')
imagetelfmind()
plateau.bind('<Button-1>', colorage)
fenetre.mainloop()
