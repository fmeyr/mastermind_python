Telfmind - Fonctionnement :

- Jeu codé en Python 3.8
- Jeu testé et fonctionnel sur IDLE (problèmes de lancement de la fonction PhotoImage sur Visual Studio Code)
- Est censé faire tourner les images "Logo.png, "Victoire.png" et le fichier audio"theo.wav" durant la partie SOUS WINDOWS 10 (pas testé ailleurs)
- Pour lancer une partie, une fois le code démarré, il suffit de se rendre dans le menu "Jeu" et de cliquer sur "Nouvelle Partie"
- Il suffit ensuite de suivre les règles du Mastermind et les instructions à l'écran

Enjoy your Game !