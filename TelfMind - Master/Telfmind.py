from tkinter import *                                   #importation de toute la bibliothèque tkinter
from random import *                                    #importation de toute la bibliothèque random
from tkinter.messagebox import  askokcancel             #importation du module askokcancel depuis la biliothèque messagebox
import os                                               #importation du module os

#Définition des variables globales

listecouleurpion = ['red3', 'coral', 'gold', 'SpringGreen2', 'DeepSkyBlue2', 'DarkOrchid2']       #liste des couleurs des pions du jeu
idlistecouleurpion = [0, 1, 2, 3, 4, 5]		       #id de la liste du dessus
listemystere = [0,0,0,0]                           #liste de la combinaison à deviner                        
listedumoment = [0, 0, 0, 0]                       #liste de la combinaison testée ce tour par le joueur
coordcercledumoment = []                           #coordonnées  des 4 cercles d'un test
nbclicga = 0                                       #nombre de clics gauche souris (sélection des couleurs)
nbessai = 1                                        #nombre de tests effectués (max 10)            
nbclicgaparessai = 0                               #nombre de clics effectués par test (on compte comme le nbclicga mais on réinitialise à chaque fois)
entraindejouer = False                             #booléen qui vérifie si on est en jeu avant de faire une action
cerclecouleur = []                                 #liste des coordonnées des cercles de couleurs définis à chaque clic pour un test
dossier = os.getcwd()                              #chemin absolu où se trouve le dossier du jeu

#Définition des fonctions d'affichage

def imagetelfmind():                               #on définie l'image qui sera affichée à l'entrée du jeu
    """ Image de base du jeu"""
    plateau.create_image (200, 350, image = logo)  #création de l'image

def imagefin():                                    #on définie l'image qui sera affichée à la fin du jeu
    """ Image de fin du jeu"""
    plateau.create_image (150, 600, image = vicabdef)

def menu(fenetre):            #on définie la barre de menu du jeu
    "Barre de menu de Telfmind"

    top=Menu(fenetre)               #on nomme le menu "top"
    fenetre.config(menu = top)      #on détermine le menu de la fenêtre pour que ce soit "top"
    J=Menu(top)                     #J est un sous-menu de Top
    top.add_cascade(label = 'Jeu', menu = J, underline = 0)     #On crée la cascade de "top" nommée "jeu"
    J.add_command(label = 'Nouvelle partie', command = jouance, underline = 0) #on crée le bouton pour une nouvelle partie à partir de la fonction de jeu (jouance)
    J.add_command(label = 'Test Ligne', command = nouveautest, underline = 0)
    J.add_command(label = 'Efface Ligne', command = nouveleffaçage, underline = 0)
    J.add_command(label='Abandonner (montrer la reponse)', command = txtabandon, underline = 0)
    J.add_command(label = 'Quitter le jeu', command = quitter, underline = 0)

#Définition des fonctions principales :

def aleatoire():        #création de la combinaison de couleur à deviner avec mise en forme : 5 couleurs max
        """ Définition aléatoire de la combinaison de couleur à deviner """
        
        idlistecouleurpion = [0, 1, 2, 3, 4, 5]			                               #assigne des fhiffres aux couleurs							  
        plateau.create_rectangle (10, 58, 190, 92, outline = 'white',fill = 'black')    #rectangle qui accueillera la combinaison
        for i in range (4):
                listemystere[i] = choice(idlistecouleurpion)                            #tirage combinaison mystère
                plateau.create_text ((i+1)*40, 76, text = "*", font = "Rockwell 16 ", fill = "white") #définition du texte qui cachera la combinaison
                idlistecouleurpion.remove (listemystere[i])                             #empêche de tomber deux fois sur la même couleur
                
def colorage(event):
    if entraindejouer:
        """ Gestion de l'événement "Clic gauche", sur un cercle, pour sélectionner une couleur """

        global nbclicgaparessai            #avec global on simplifie en abolissant la notion de variable locale
        global nbclicga
        if nbclicga >=6: nbclicga = 0      #Si le choix du joueur dépasse la sixième couleur, on retourne à la première
        global cerclecouleur 
        #position de la souris
        X = event.x
        Y = event.y
        #Identification du cercle cliqué et sélection de couleur
        for i in range (4):
                if (coordcercledumoment [i][0] <= X <= coordcercledumoment [i][2]) and (coordcercledumoment [i][1] <= Y <= coordcercledumoment [i][3]):
                        cerclecouleur.append(plateau.create_oval (coordcercledumoment[i], fill = listecouleurpion[nbclicga]))
                        listedumoment[i] = nbclicga    #permet la corolation en fonction du nombre de clic
        nbclicga = nbclicga+1
        nbclicgaparessai = nbclicgaparessai + 1

def jouance():
        """ Gestion d'une partie de jeu avec création de cercles et d'une combinaison mystère"""

        plateau.delete(ALL)             #suppression interface jeu
        global nbessai                  #Gestion des variables pour une nouvelle partie
        global nbclicga
        global nbclicgaparessai
        nbclicga = 0
        nbessai = 1					
        nbclicgaparessai = 0
        creercercle(nbessai)            #création de la première ligne de jeu
        aleatoire()				        #combinaison mystère
        global entraindejouer
        entraindejouer = True			#on active la possibilité de jouer
        plateau.create_text(90, 32, text = "Mot de Passe", font = "Rockwell 16 ", fill = "white")       #texte des colonnes de jeu
        plateau.create_text(290, 32, text = "Indice", font = "Rockwell 16", fill = "white") 

def creercercle(nbessai):
    """ Création des cercles du test, avant leur coloration"""
    
    for i in range (4):
            cercle = plateau.create_oval ((i+1)*40-15, nbessai*35+70, (i+1)*40+15, nbessai*35+100, outline = "white")
            coordcercledumoment.insert (i, plateau.coords (cercle))    #lorsqu'un cercle d'un essai est créé, on rentre ses coordonnées dans la liste pour le colorer plus tard

def nouveautest():      #compare la ligne avec la mystère et crée 4 nouveaux cercles en-dessous
    """ Délenche le test de la ligne remplie ce tour"""

    global nbclicgaparessai
    global cerclecouleur
    if entraindejouer == True and nbclicgaparessai >= 4:    #vérification d'au moins 4 clics
        global nbessai
        nbessai = nbessai+1             #puisque le joueur a testé, on incrémente de 1
        if nbessai > 10:                #si le nombre de tests dépasse ce qu'il a le droit, le joueur perd
            txtdefaite()
        creercercle(nbessai)            #crée 4 cercles en dessous des premiers
        indication()                    #comparaison entre les deux listes de couleur
        nbclicgaparessai = 0            #réinitialise les clics et cercles enregistrés pour le test suivant
        cerclecouleur = []

def nouveleffaçage():          #permet au bouton d'effaçage de rendre à nouveau la ligne vierge
    """ Définit l'effaçage de la ligne remplie ce tour"""

    global nbclicgaparessai
    nbclicgaparessai = 0        #remet à zéro le compteur pour empêcher de jouer sur une ligne vide
    global cerclecouleur
    i = 0                       #variable pour passer de cercles en cercles
    while cerclecouleur != []:  #tant qu'il y a des cercles de couleurs qui ont été définis en cliquant, on va les reconfigurer
        try :
            plateau.itemconfigure (cerclecouleur[i], fill = 'slate blue', outline = 'white')   #on reconfigure nos cercles créés en cliquant pour leur donner l'apparence de base
            cerclecouleur.pop (0)           #on vire les cercles déjà reconfigurés de la liste 
        except ValueError:                  #S'il n'est plus ou pas possible de reconfigurer, on print une erreur
            print ("la valeur est vide")

def indication():       #créer des cercles dans la colonne indice en fonction de la comparaison de listes 
    """ Permet d'afficher les indices pour le joueur"""

    global nbclicga
    valide = 0          #on crée une valeur qui va comptabiliser le nombre de cercles "justes" dans la ligne
    j = 0               #permet de pas avoir les ronds d'indice à la même place que les premiers 
    for i in range(4):      #gestion des cas possible
        if listedumoment[i] in listemystere :
            if listedumoment[i] == listemystere[i] :    #Si la case testée correspond à une case mystère, créer un cercle rouge
                plateau.create_oval ((j+6)*40-15, (nbessai-1)*35+70, (j+6)*40+15, (nbessai - 1)*35+100, fill = "red")
                valide = valide + 1         #on incrémente de 1 le nombre de valeurs justes
                j = j + 1
            if listedumoment[i] in listemystere and listedumoment[i] != listemystere[i]:        #si le rond de la case est présent dans une case mystère mais pas au bon emplacement, rond blanc
                plateau.create_oval ((j+6)*40-15, (nbessai-1)*35+70, (j+6)*40+15, (nbessai - 1)*35+100, fill = "white")
                j = j + 1
        if valide == 4:     #si toutes les cases de la proposition sont bonnes, on délenche la victoire
            txtvictoire()
    #   à noter : la disposition des cercles ne correspondra pas forcément à ce qui a été rempli sur le tableau, non pas car la création est aléatoire,
    #   mais bien parce qu'elle dépend de l'ordre de vérification et des clics effectués (elle suit néanmoins donc une certaine logique)

def findepartie():
    """ Pour déclarer la fin de la partie et révèler la combinaison """

    global entraindejouer
    if entraindejouer == True:                              #Lorsque la fonction est déclenché, on crée les cercles qui affichent la combinaison mystère de la partie
        for i in range (len(listemystere)):                 
            plateau.create_oval ((i+1)*40-15, 60, (i+1)*40+15, 90, fill = listecouleurpion[listemystere[i]])
        entraindejouer = False                              #Puis on arrête la possibilité de jouer et on affiche l'image de fin
        imagefin() 

def rejouance():
    """ Message box pour rejouer une partie"""

    repartie = askokcancel('TelfMind',"Je crois que la partie est finie ! Alors, on se fait une petite revanche ?")  #on utilise la message box type askokancel pour laisser un choix fermé à l'utilisateur
    if repartie:            #si l'utilisateur dit "ok", une nouvelle partie se lance
        jouance()

def musique_de_jeu():
    """ Musique qui sera lue durant l'execution du script et les parties jouées"""
    try:                        #si l'ordinateur est sous windows et/ou le supporte, on tente de lancer la musique
        import winsound                     #on importe le module nécessaire "winsound"
        musique = dossier + '/theo.wav'            #on définit l'emplacement absolu du fichier de musique (pas de relatifs comme ça)
        winsound.PlaySound(musique, winsound.SND_FILENAME | winsound.SND_ASYNC)     #déclenche la lecture de la musique en parallèle du jeu
    except ValueError:          #si impossible de la lancer, message d'erreur correspondant
        print("Vous n'êtes pas sur Windows, la musique du jeu ne pourra pas se lancer. Mais vous pouvez quand même jouer ! Bonne chance ;)")

def txtvictoire():
    """ Fonction délenchée en cas de victoire de la partie"""

    findepartie()           #fonction qui clôt la partie
    plateau.create_text(200, 720, text = "Incroyable ! Tu es vraiment le plus fort !", font = "Rockwell 13", fill = "white")        #message de victoire
    rejouance()             #possibilté de rejouer

def txtdefaite():       #idem que plus haut
    """ Message délenché en cas de défaite de la partie"""

    findepartie()
    plateau.create_text(200, 720, text = "Trop d'essais tue l'essai, tu sais ? Tu as perdu !", font = "Rockwell 13 ", fill = "white") 
    rejouance()

def txtabandon():       #idem que plus haut
    """ Message délenché en cas d'abandon de la partie"""

    findepartie()
    plateau.create_text(200, 720, text = "Tu abandonnes ? Quel dommage... voilà ta réponse.", font = "Rockwell 12 ", fill = "white") 
    rejouance()

def quitter():
    """ Message box pour quitter le jeu"""

    quittage = askokcancel('TelfMind',"Tu es sûr de vouloir quitter la partie ? On s'amuse bien pourtant !") #même principe que rejouance
    if quittage:
        fenetre.destroy() #détruire la fenêtre de jeu
        exit()            #quitter la console

#fonctions de lancement de la fenêtre de jeu (graphisme tkinter)

fenetre = Tk()  #on donne la bibliothèque tkinter à notre fenêtre de jeu
fenetre.title('TelfMind, le plus grand de tous les Master')     #titre de la fenêtre
plateau = Canvas(fenetre, height = 750,width = 400, bg ='slate blue' )      #définition taille et couleur du plateau de jeu
plateau.pack(side = RIGHT, padx = 0, pady = 0)                  #inclusion du plateau dans la fenêtre
contreplateau = Canvas(fenetre, height = 750, width = 200, bg ='gray20')    #définition espace gauche secondaire pour les boutons de jeu
contreplateau.pack(side = LEFT)                                 #inclusion dans la fenêtre
selObject = contreplateau                                       #sélection de l'espace
testBouton = Button(contreplateau, text = ("Test Ligne"), command = nouveautest)    #définition et inclusion bouton test dans espace de gauche
testBouton.pack(side = TOP, padx = 0, pady = 364) 
effaceBouton = Button(contreplateau, text = ("Efface Ligne"), command = nouveleffaçage)     #idem pour le bouton effaçage
effaceBouton.pack(side = LEFT)
menu(fenetre)                                                   #on donne le menu "Jeu" à la fenêtre
image1 = dossier + '/Logo.png'                                  #on rentre l'emplacement absolu d'une image du jeu dans une variable
image2 = dossier + '/victoire.png'
logo = PhotoImage(file = image1)                                #définition des images "logo" et "vicabdef"
vicabdef = PhotoImage(file = image2)
imagetelfmind()                                                 #on fait démarrer le jeu sur l'image principale
plateau.bind('<Button-1>', colorage)                            #on fait en sorte que l'évènement click gauche de la souris soit associé à la fonction de coloration des cercles
musique_de_jeu()                                                #on lance la musique du jeu
fenetre.mainloop()                                              #on affiche la fenêtre dès que tout est prêt
